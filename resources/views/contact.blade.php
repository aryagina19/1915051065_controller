@extends('template')
@section('header')

   <!-- Fixed Navigation Starts -->
   <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
    <li class="icon-box">
        <i class="fa fa-home"></i>
        <a href="/">
            <h2>Home</h2>
        </a>
    </li>
    <li class="icon-box">
        <i class="fa fa-user"></i>
        <a href="/about">
            <h2>About</h2>
        </a>
    <li class="icon-box">
        <i class="fa fa-user"></i>
        <a href="/portfolio">
            <h2>Portfolio</h2>
        </a>
    <li class="icon-box active">
        <i class="fa fa-envelope-open"></i>
        <a href="/contact">
            <h2>Contact</h2>
        </a>
    </li>
</ul>
<!-- Fixed Navigation Ends -->
<!-- Mobile Menu Starts -->
<nav role="navigation" class="d-block d-lg-none">
    <div id="menuToggle">
        <input type="checkbox" />
        <span></span>
        <span></span>
        <span></span>
        <ul class="list-unstyled" id="menu">
            <li><a href="/"><i class="fa fa-home"></i><span>Home</span></a></li>
            <li><a href="/about"><i class="fa fa-user"></i><span>About</span></a></li>
            <li><a href="/portfolio"><i class="fa fa-folder-open"></i><span>Portfolio</span></a></li>
            <li class="active"><a href="/contact"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>
        </ul>
    </div>
</nav>
<!-- Mobile Menu Ends -->
@endsection
@section('layout')
    
<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1><span>Kontak</span></h1>
    <span class="title-bg">Selalu Terhubung</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Left Side Starts -->
            <div class="col-12 col-lg-4">
                <h3 class="text-uppercase custom-title mb-0 ft-wt-600 pb-3">Mari Berteman</h3>
                <p class="open-sans-font mb-3">Mari saling mengenal lebih dekat. Saya selalu terbuka dan ingin tahu jika diajak dalam berdiskusi bersama mengenai seputaran IT</p>
                <p class="open-sans-font custom-span-contact position-relative">
                    <i class="fa fa-envelope-open position-absolute"></i>
                    <span class="d-block">Kirim Pesan Melalui</span>aryagina19@gmail.com
                </p>
                <p class="open-sans-font custom-span-contact position-relative">
                    <i class="fa fa-phone-square position-absolute"></i>
                    <span class="d-block">Hubungi Saya</span>+6282494614235
                </p>
                <ul class="social list-unstyled pt-1 mb-5">
                    <li class="facebook"><a title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
                    </li>
                    <li class="twitter"><a title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                    </li>
                    <li class="youtube"><a title="Youtube" href="#"><i class="fa fa-youtube"></i></a>
                    </li>
                    <li class="dribbble"><a title="Dribbble" href="#"><i class="fa fa-dribbble"></i></a>
                    </li>
                </ul>
            </div>
            <!-- Left Side Ends -->
            <!-- Contact Form Starts -->
            <div class="col-12 col-lg-8">
                <form class="contactform" method="post" action="http://slimhamdi.net/tunis/dark/php/process-form.php">
                    <div class="contactform">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <input type="text" name="name" placeholder="Nama Anda">
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="email" name="email" placeholder="E-mail Anda">
                            </div>
                            <div class="col-12 col-md-4">
                                <input type="text" name="subject" placeholder="Subjek atau Judul">
                            </div>
                            <div class="col-12">
                                <textarea name="message" placeholder="Pesan Anda"></textarea>
                                <button type="submit" class="btn btn-contact">Kirim Pesan</button>
                            </div>
                            <div class="col-12 form-message">
                                <span class="output_message text-center font-weight-600 text-uppercase"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Contact Form Ends -->
        </div>
    </div>

</section>
<!-- Template JS Files -->
<script src="js/jquery-3.5.0.min.js"></script>
<script src="js/styleswitcher.js"></script>
<script src="js/preloader.min.js"></script>
<script src="js/fm.revealator.jquery.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpGridGallery.js"></script>
<script src="js/jquery.hoverdir.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/custom.js"></script>

</body>


<!-- Mirrored from slimhamdi.net/tunis/dark/contact.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Mar 2021 11:57:28 GMT -->
</html>

@endsection
