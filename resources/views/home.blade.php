﻿@extends('template')
@section('header')
   <!-- Fixed Navigation Starts -->
   <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
    <li class="icon-box active">
        <i class="fa fa-home"></i>
        <a href="/">
            <h2>Home</h2>
        </a>
    </li>
    <li class="icon-box">
        <i class="fa fa-user"></i>
        <a href="/about">
            <h2>About</h2>
        </a>
    <li class="icon-box">
        <i class="fa fa-user"></i>
        <a href="/portfolio">
            <h2>Portfolio</h2>
        </a>
    <li class="icon-box">
        <i class="fa fa-envelope-open"></i>
        <a href="/contact">
            <h2>Contact</h2>
        </a>
    </li>
</ul>
<!-- Fixed Navigation Ends -->
<!-- Mobile Menu Starts -->
<nav role="navigation" class="d-block d-lg-none">
    <div id="menuToggle">
        <input type="checkbox" />
        <span></span>
        <span></span>
        <span></span>
        <ul class="list-unstyled" id="menu">
            <li class="active"><a href="/"><i class="fa fa-home"></i><span>Home</span></a></li>
            <li><a href="/about"><i class="fa fa-user"></i><span>About</span></a></li>
            <li><a href="/portfolio"><i class="fa fa-folder-open"></i><span>Portfolio</span></a></li>
            <li><a href="/contact"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>
        </ul>
    </div>
</nav>
<!-- Mobile Menu Ends -->
@endsection
@section('layout')
    
<!-- Main Content Starts -->
<section class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
    <div class="color-block d-none d-lg-block"></div>
    <div class="row home-details-container align-items-center">
        <div style="background-image: url(../public/img/img-mobile.jpg);"></div>
        <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div>
        <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
            <div>
                <img src="img/img-mobile.jpg" class="img-fluid main-img-mobile d-none d-sm-block d-lg-none" alt="my picture" />
                <h6 class="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">Salam Kenal</h6>
                <h1 class="text-uppercase poppins-font"><span>Saya</span> Arya Gina </h1>
                <p class="open-sans-font">Saya adalah Mahasiswa bidang studi PTI di jurusan Teknik Informatika Universitas Pendidikan Ganesha yang ingin mencari banyak ilmu dan pengalaman seputar IT</p>
                <a href="/about" class="btn btn-about">Beberapa tentang saya</a>
            </div>
        </div>
    </div>
</section>
<!-- Main Content Ends -->

<!-- Template JS Files -->
<script src="js/jquery-3.5.0.min.js"></script>
<script src="js/styleswitcher.js"></script>
<script src="js/preloader.min.js"></script>
<script src="js/fm.revealator.jquery.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpGridGallery.js"></script>
<script src="js/jquery.hoverdir.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/custom.js"></script>

@endsection