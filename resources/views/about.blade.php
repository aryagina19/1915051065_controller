@extends('template')
@section('header')
   <!-- Fixed Navigation Starts -->
   <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
    <li class="icon-box">
        <i class="fa fa-home"></i>
        <a href="/">
            <h2>Home</h2>
        </a>
    </li>
    <li class="icon-box active">
        <i class="fa fa-user"></i>
        <a href="/about">
            <h2>About</h2>
        </a>
    <li class="icon-box">
        <i class="fa fa-user"></i>
        <a href="/portfolio">
            <h2>Portfolio</h2>
        </a>
    <li class="icon-box">
        <i class="fa fa-envelope-open"></i>
        <a href="/contact">
            <h2>Contact</h2>
        </a>
    </li>
</ul>
<!-- Fixed Navigation Ends -->
<!-- Mobile Menu Starts -->
<nav role="navigation" class="d-block d-lg-none">
    <div id="menuToggle">
        <input type="checkbox" />
        <span></span>
        <span></span>
        <span></span>
        <ul class="list-unstyled" id="menu">
            <li><a href="/"><i class="fa fa-home"></i><span>Home</span></a></li>
            <li class="active"><a href="/about"><i class="fa fa-user"></i><span>About</span></a></li>
            <li><a href="/portfolio"><i class="fa fa-folder-open"></i><span>Portfolio</span></a></li>
            <li><a href="/contact"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>
        </ul>
    </div>
</nav>
<!-- Mobile Menu Ends -->
    
@endsection

@section('layout')

<!-- Page Title Starts -->
<section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
    <h1>TENTANG <span>SAYA</span></h1>
    <span class="title-bg">Resume</span>
</section>
<!-- Page Title Ends -->
<!-- Main Content Starts -->
<section class="main-content revealator-slideup revealator-once revealator-delay1">
    <div class="container">
        <div class="row">
            <!-- Personal Info Starts -->
            <div class="col-12 col-lg-5 col-xl-6">
                <div class="row">
                    <div class="col-12">
                        <h3 class="text-uppercase custom-title mb-0 ft-wt-600">Info Personal</h3>
                    </div>
                    <div class="col-12 d-block d-sm-none">
                        <img src="img/img-mobile.jpg" class="img-fluid main-img-mobile" alt="my picture" />
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Nama Depan :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Arya</span> </li>
                            <li> <span class="title">Nama Belakang :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Gina</span> </li>
                            <li> <span class="title">Umur :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">20 Tahun</span> </li>
                            <li> <span class="title">Kewarganegaraan :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia</span> </li>
                            <li> <span class="title">Status :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Mahasiswa</span> </li>
                        </ul>
                    </div>
                    <div class="col-6">
                        <ul class="about-list list-unstyled open-sans-font">
                            <li> <span class="title">Alamat :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Denpasar</span> </li>
                            <li> <span class="title">Phone :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">+6282494614235</span> </li>
                            <li> <span class="title">E-mail :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">aryagina19@gmail.com</span> </li>
                            <li> <span class="title">Instagram :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">arya_gina</span> </li>
                            <li> <span class="title">Bahasa :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia, Inggris</span> </li>
                        </ul>
                    </div>
                    <div class="col-12 mt-3">
                        <a href="/about" class="btn btn-download">Download CV</a>
                    </div>
                </div>
            </div>
            <!-- Personal Info Ends -->
            <!-- Boxes Starts -->
            <div class="col-12 col-lg-7 col-xl-6 mt-5 mt-lg-0">
                <div class="row">
                    <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">12 Tahun</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Dalam Menyelesaikan  <span class="d-block">Pendidikan</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats with-margin">
                            <h3 class="poppins-font position-relative">40</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Piagam <span class="d-block">yang dimiliki</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">2 Tahun</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Menjadi<span class="d-block">Mahasiswa</span></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="box-stats">
                            <h3 class="poppins-font position-relative">12</h3>
                            <p class="open-sans-font m-0 position-relative text-uppercase">Kegiatan yang <span class="d-block">Diikuti</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Boxes Ends -->
        </div>
        <hr class="separator">
        <!-- Skills Starts -->
        <div class="row">
            <div class="col-12">
                <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">Skill yang Dimiliki</h3>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p25">
                    <span>25%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">PHP</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p89">
                    <span>89%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Adobe Ilustrator</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p70">
                    <span>70%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">HTML</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p66">
                    <span>66%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">CSS</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p95">
                    <span>95%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Canva</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p50">
                    <span>50%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Adobe Premiere</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p65">
                    <span>65%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Adobe Lightroom</h6>
            </div>
            <div class="col-6 col-md-3 mb-3 mb-sm-5">
                <div class="c100 p45">
                    <span>45%</span>
                    <div class="slice">
                        <div class="bar"></div>
                        <div class="fill"></div>
                    </div>
                </div>
                <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">Figma</h6>
            </div>
        </div>
        <!-- Skills Ends -->
        <hr class="separator mt-1">
</section>
<!-- Main Content Ends -->

<!-- Template JS Files -->
<script src="js/jquery-3.5.0.min.js"></script>
<script src="js/styleswitcher.js"></script>
<script src="js/preloader.min.js"></script>
<script src="js/fm.revealator.jquery.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpGridGallery.js"></script>
<script src="js/jquery.hoverdir.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/custom.js"></script>

</body>


<!-- Mirrored from slimhamdi.net/tunis/dark/about.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 24 Mar 2021 11:59:27 GMT -->
</html>

@endsection